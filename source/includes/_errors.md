# Errors

<aside class="notice">This section (well, every section) is a work-in-progress.</aside>

The TgmRank API uses the following error codes:


Error Code | Meaning
---------- | -------
400 | Your request sucks
404 | Where you at?
500 | Sorry, my fault
