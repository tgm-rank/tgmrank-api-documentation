---
title: API Reference

language_tabs:
  - json response

toc_footers:
  - Placeholder
  - <a href='http://github.com/mpociot/whiteboard'>Documentation Powered by Whiteboard</a>

includes:
  - errors

search: true
---

# Introduction

Documentation for the Tgm Rank API.

# Games

## Get all games

`GET https://theabsolute.plus/api/game`

```json
[
  {
    "game_id": 0,
    "game_name": "Overall",
    "short_name": "Overall",
    "subtitle": "",
    "modes": []
  },
  {
    "game_id": 1,
    "game_name": "Tetris The Grandmaster",
    "short_name": "TGM1",
    "subtitle": "テトリス　ザ・グランドマスター",
    "modes": [
      {
        "mode_id": 1,
        "game_id": 1,
        "mode_name": "Master",
        "is_ranked": true,
        "link": "https://tetrisconcept.net/threads/tgm1.134/"
      }    
    ]
  }
]
```

This endpoint returns a list of all games.

<aside class="notice">This list by default also includes "Overall", which is a virtual game.</aside>

## Get game by id

`GET https://theabsolute.plus/api/game/<game_id>`

```json
{
  "game_id": 1,
  "game_name": "Tetris The Grandmaster",
  "short_name": "TGM1",
  "subtitle": "テトリス　ザ・グランドマスター",
  "modes": [
    {
      "mode_id": 1,
      "game_id": 1,
      "mode_name": "Master",
      "is_ranked": true,
      "link": "https://tetrisconcept.net/threads/tgm1.134/"
    }
  ]
}
```

# Modes

## Get mode by id

`GET https://theabsolute.plus/api/mode/<mode_id>`

```json
{
  "mode_id": 1,
  "game_id": 1,
  "mode_name": "Master",
  "is_ranked": true,
  "link": "https://tetrisconcept.net/threads/tgm1.134/"
}
```

# Rankings

## Get all aggregated rankings

`GET https://theabsolute.plus/api/ranking`

```json
[
  {
    "rank_id": 1,
    "game_id": 0,
    "player_name": "Player 1",
    "rank": 1,
    "ranking_points": 590
  },
  {
    "rank_id": 2,
    "game_id": 0,
    "player_name": "Player 2",
    "rank": 2,
    "ranking_points": 581
  },
]
```

<aside class="notice">This is a flat list of simple ranking data by game.</aside>

The response should probably be grouped... To Do?

## Get aggregated ranking by game id

`GET https://theabsolute.plus/api/ranking/<game_id>`

```json
[
  {
    "rank_id": 1,
    "game_id": 0,
    "player_name": "Player 1",
    "rank": 1,
    "ranking_points": 590
  },
  {
    "rank_id": 2,
    "game_id": 0,
    "player_name": "Player 2",
    "rank": 2,
    "ranking_points": 581
  },
]
```

## Get full ranking by game id and mode id

`GET https://theabsolute.plus/api/ranking/<game_id>/mode/<mode_id>`

```json
[
  {
    "score_id": 1,
    "level": 999,
    "playtime": "00:09:11.410",
    "grade_display": "GM",
    "player_name": "K",
    "rank": 1,
    "ranking_points": 100
  },
  {
    "score_id": 2,
    "level": 999,
    "playtime": "00:09:19.350",
    "grade_display": "GM",
    "player_name": "KevinDDR",
    "rank": 2,
    "ranking_points": 99
  },
  {
    "score_id": 3,
    "level": 999,
    "playtime": "00:09:25.060",
    "grade_display": "GM",
    "player_name": "MHL",
    "rank": 3,
    "ranking_points": 98
  }
]
```

<aside class="notice">This response looks different compared to the previous two, since now we're actually looking at scores instead of players' scores across multiple games/modes.</aside>

The response should probably look the same, and there should be a different endpoint or query string parameter to show the different stuff.

# Scores

## Get score by id

`GET https://theabsolute.plus/api/score/<score_id>`

```json
{
  "score_id": 1,
  "grade_id": 1,
  "level": 999,
  "playtime": "00:09:11.410",
  "score": null,
  "proof": "https://tetrisconcept.net/attachments/img_20181031_164029-jpg.1324/",
  "status": "legacy",
  "comment": "Sorry dudes, no video for that one... :V",
  "player_id": 2890,
  "game_id": 34,
  "mode_id": 265,
  "created_at": "2019-01-01 08:02:52.191233",
  "updated_at": "2019-01-01 08:02:52.191233"
}
```

This should at least include ranking data. Subobjects for the score's associated game, mode, grade, and player info?

# Player Scores

## Get all scores for player

`GET https://theabsolute.plus/api/player/<player_id>`

```json
{
  "player_id": 1234,
  "player_name": "Player 1",
  "games": [
    {
      "game_id": 1,
      "game_name": "Tetris The Grandmaster",
      "short_name": "TGM1",
      "subtitle": "テトリス　ザ・グランドマスター",
      "modes": [
        {
          "mode_id": 1,
          "mode_name": "Master",
          "scores": [
            {
              "score_id": 5145,
              "level": 999,
              "playtime": "00:09:11.410",
              "grade_display": "GM",
              "rank": 1,
              "ranking_points": 100
            }
          ]
        }
      ]
    }
  ]
}
```

## Get scores for player filtered by game id

`GET https://theabsolute.plus/api/player/<player_id>/game/<game_id>`

```json
<Same format as the above>
```

## Get scores for player filtered by game id and mode id

`GET https://theabsolute.plus/api/player/<player_id>/game/<game_id>/mode/<mode_id>`

```json
<Same format as the above>
```

# Grades

## Get all possible grades for mode

`GET https://theabsolute.plus/api/grade/for/mode/<mode_id>`

```json
[
  {
    "grade_id": 1,
    "grade_display": "GM"
  }
  {
    "grade_id": 2,
    "grade_display": "S9"
  },
  ...
]
```

## Get grade by id

`GET https://theabsolute.plus/api/grade/<grade_id>`

```json
{
  "grade_id": 1,
  "grade_display": "GM"
}
```
